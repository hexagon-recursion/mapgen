function getRandomInt(min, max) {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random#getting_a_random_integer_between_two_values
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}

function generate_world({
  worldHeight,
  worldWidth,
  numberOfBlobs,
  minBlobSize,
  maxBlobSize,
}) {
  var r = "rgb(200, 0, 0)";
  var g = "rgb(0, 200, 0)";
  var b = "rgb(0, 0, 200)";
  var world = Array(worldHeight)
    .fill()
    .map(() => Array(worldWidth).fill(b));

  for (var i = 0; i < numberOfBlobs; ++i) {
    var size = getRandomInt(minBlobSize, maxBlobSize);
    var top = getRandomInt(0, worldHeight - size);
    var left = getRandomInt(0, worldWidth - size);
    for (var x = left; x < left + size; ++x) {
      for (var y = top; y < top + size; ++y) {
        world[y][x] = g;
      }
    }
  }

  // 0, 0 is top left
  world[0][0] = r;

  return world;
}

function draw(world) {
  var canvas = document.getElementById("output");
  if (canvas.getContext) {
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var tileSize = 10;
    var x, y;

    y = 0;
    for (var row of world) {
      x = 0;
      for (var tile of row) {
        ctx.fillStyle = tile;
        ctx.fillRect(x, y, tileSize, tileSize);
        x += tileSize;
      }
      y += tileSize;
    }
  }
}

function reset() {
  var controls = Array.from(
    document.querySelectorAll(".js-controls-canvas.js-int")
  );
  var config = Object.fromEntries(controls.map((c) => [c.id, +c.value]));
  var w = generate_world(config);
  draw(w);
}

window.addEventListener("load", () => {
  for (var e of document.querySelectorAll(".js-controls-canvas")) {
    e.addEventListener("input", () => {
      reset();
    });
  }
  reset();
});
